module.exports = {
  assetsDir: 'assets',
  runtimeCompiler: true,
  lintOnSave: false
}
