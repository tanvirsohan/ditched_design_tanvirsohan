import Home from './components/pages/Home.vue';
import About from './components/pages/About.vue';
import Skills from './components/pages/Skills.vue';
import Experience from './components/pages/Experience.vue';
import Showcase from './components/pages/Showcase.vue';
import Contact from './components/pages/Contact.vue';

export const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/about',
        component: About
    },
    {
        path: '/skills',
        component: Skills
    },
    {
        path: '/experience',
        component: Experience
    },
    {
        path: '/showcase',
        component: Showcase
    },
    {
        path: '/contact',
        component: Contact
    }
];
