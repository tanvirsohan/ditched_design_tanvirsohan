require("./bootstrap");

// vue
import Vue from "vue";

// vue router
import VueRouter from "vue-router";
import {routes} from "./routes";

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history'
});

// vuetify
import vuetify from "./vuetify";

// components
import App from "./components/App.vue";

new Vue({
    el: "#app",
    router,
    vuetify,
    components: {
        App
    },
    render: h => h(App),
});
